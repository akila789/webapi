﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Application.Data;
using System.Data;

namespace Application.Service.Services
{
    public class ProductService : IProductService
    {
        private ProductDBEntities context = new ProductDBEntities();



        public Product GetProduct(int id)
        {
            return context.Products.Where(x => x.Id == id).FirstOrDefault();
        }

        public List<Product> GetProducts()
        {
            return context.Products.ToList();
        }

        public bool SaveProduct(Product p)
        {
            try
            {
                context.Products.Add(p);
                context.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public bool UpdateProduct(int id, Product p)
        {
            try
            {
                context.Entry(p).State = System.Data.Entity.EntityState.Modified;
                context.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool DeleteProduct(int productId)
        {
            try
            {
                var product = context.Products.Where(x => x.Id == productId).FirstOrDefault();
                if (product == null) return false;
                context.Entry(product).State = System.Data.Entity.EntityState.Deleted;
                context.SaveChanges();
                return true;

            }catch (Exception e)
            {
                return false;
            }
        }


    }
}