﻿using Application.Data;
using Application.Service.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Application.Data;

namespace Application.WebApi.Controllers
{
    public class ProductController : ApiController
    {
        private IProductService productService = new ProductService();

        [HttpGet]
        public IEnumerable<Product> GetProducts()
        {
            return productService.GetProducts();
        }

        [HttpGet]
        [ActionName("GetProduct")]
        public IHttpActionResult GetStoreProduct(int id)
        {
            var product = productService.GetProduct(id);
            if (product == null)
            {
                return NotFound();
            }
            return Ok(product);
        }


        [HttpPost]
        public IHttpActionResult Post(Product p)
        {
           var isSave =  productService.SaveProduct(p);
            if (isSave == true)
                return Ok();
            return BadRequest();
        }

        [HttpPut]
        public IHttpActionResult Put(Product p)
        {
            var isUpdated = productService.UpdateProduct(p.Id, p);
            if (isUpdated == true)
                    return Ok();
            return BadRequest();
        }

        [HttpDelete]
        public IHttpActionResult Delete(int id)
        {
            var isDeleted = productService.DeleteProduct(id);
            if (isDeleted==true)
            {
                return Ok();
            }
            return BadRequest();
        }
    } 
}
