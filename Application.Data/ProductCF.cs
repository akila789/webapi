﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Application.Data
{
    public class ProductCF
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool Gender { get; set; } //male 1 female 0
        public int Qty { get; set; }
    }
}